﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager current;
    public event Action OnStateChange;
    public event Action OnUpdatePlayerStats;
    public event Action OnClickOnScreen;
    public event Action<int> OnCollectTriggerEnter;

    public GameState GameState { get; private set; }

    private void Awake()
    {
        current = this;
        GameState = GameState.BeforeStart;
    }

    // Start is called before the first frame update
    void Start()
    {
        //TODO Initilaze PlayerStats
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState == GameState.BeforeStart)
        {
            //Debug.Log("BeforeStart");
        }
        else
        {
            //Debug.Log("Changed");
        }
    }

    public void SetGameState(GameState gameState)
    {
        this.GameState = gameState;
        OnStateChange?.Invoke();
    }

    public void UpdatePlayerStats()
    {
        OnUpdatePlayerStats?.Invoke();
    }

    public void Collect(int value)
    {
        OnCollectTriggerEnter?.Invoke(value);
    }

    public void IncreaseCurrencyOnClick()
    {
        OnClickOnScreen?.Invoke();
    }
}
