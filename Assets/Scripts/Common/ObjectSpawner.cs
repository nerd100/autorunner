﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public Vector3 spawnPosition = new Vector3(0, 0, 0);
    public float spawnRate = 0.0f;
    public bool spawnTrigger = true;
    public bool IsContainerReady = false;

    public ObjectPoolContainer objectPoolContainer;
    public PlayerStatScriptableObject objectSpawnRate;
    public PlayerStatScriptableObject upgradeChance;

    public Material normalCrystalMat;
    public Material upgradedCrystalMat;
    

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.transform.position = spawnPosition;
        ObjectPooler.SharedInstance.CreatePoolForGameObject(objectPoolContainer, this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        spawnRate = objectSpawnRate.Value;

        if(this.gameObject.transform.childCount > 0)
        {
            //gameObjectContainer = ObjectPooler.SharedInstance.containerPool;
            IsContainerReady = true;
        }

        if (spawnTrigger && IsContainerReady)
        {
            StartCoroutine(SpawnObjectWithCooldown(spawnRate));
        }
    }

    public IEnumerator SpawnObjectWithCooldown(float coolDown)
    {
        spawnTrigger = false;
        yield return new WaitForSeconds((100 - spawnRate) / 100);
        var inactiveGameobject = FindNextInactiveGameObject();

        if(inactiveGameobject != null)
        {         
            var currGameobject = inactiveGameobject.gameObject;
            if (IsUpgraded())
            {
                currGameobject.GetComponent<ObjectMovement>().upgraded = true;
                currGameobject.GetComponent<ObjectMovement>().value = 100;
                currGameobject.GetComponent<Renderer>().material = upgradedCrystalMat;
            }
            else
            {
                currGameobject.GetComponent<ObjectMovement>().upgraded = false;
                currGameobject.GetComponent<Renderer>().material = normalCrystalMat;
                currGameobject.GetComponent<ObjectMovement>().value = 1;
            }
        
            inactiveGameobject.SetActive(true);
        }
        spawnTrigger = true;
    }

    public GameObject FindNextInactiveGameObject()
    {
        GameObject nextInactiveGameObject = null;
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            nextInactiveGameObject = this.gameObject.transform.GetChild(i).gameObject;
            if (!nextInactiveGameObject.activeInHierarchy)
            {
                nextInactiveGameObject.transform.position = this.gameObject.transform.position;
                return nextInactiveGameObject;
            }
        }

        ObjectPooler.SharedInstance.ExtendPoolForGameObject(objectPoolContainer, this.gameObject, 5);
       
        return null;
    }

    public bool IsUpgraded()
    {
        var randomNumber = Random.Range(0, 100);

        if(randomNumber <= upgradeChance.Value)
        {
            return true;
        }
        return false;
    }
}
