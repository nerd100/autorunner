using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnRate", order = 1)]
public class SpawnRateScriptableObject : ScriptableObject
{
    public float spawnRate;
}