﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Rotation_Script : MonoBehaviour
{
    public GameObject playerObject;
    public Vector3 objectPosition = new Vector3(0, -20, -100);
    private GameObject currentPlayerObject;
    public int ObjectScale = 30;
    private float rotationY = -140;

    // Start is called before the first frame update
    void Start()
    {
        currentPlayerObject = Instantiate(playerObject, Vector3.zero, Quaternion.identity, this.gameObject.transform);
        currentPlayerObject.transform.localPosition = objectPosition;
        currentPlayerObject.transform.localRotation = Quaternion.Euler(0, rotationY, 0);
        currentPlayerObject.transform.localScale = new Vector3(ObjectScale, ObjectScale, ObjectScale);
    }

    // Update is called once per frame
    void Update()
    {
        rotationY += 1;
        currentPlayerObject.transform.localRotation = Quaternion.Euler(0, rotationY, 0);
    }
}
