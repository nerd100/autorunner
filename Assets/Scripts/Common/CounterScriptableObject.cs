using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Counter", order = 2)]
public class CounterScriptableObject : ScriptableObject
{
    public int Number;
}