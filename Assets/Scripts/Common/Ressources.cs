﻿
public class Ressources
{
    public static string currentValue = "Current Value: {0}";
    public static string currentSpeed = "Current Speed: {0}";
    public static string currentSpawnRate = "Current Spawn Rate: {0}";
    public static string currentCrystalUpgradeChance = "Current Chance: {0}";
    public static string currentCrystalMultiplier = "Current Multiplier: {0}";

    public static string currentCosts = "Costs: {0}";
    public static string currentSpeedCosts = "Costs: {0}";
    public static string currentSpawnRateCosts = "Costs: {0}";
    public static string currentCrystalUpgradeChanceCosts = "Costs: {0}";
    public static string currentCrystalMultiplierCosts = "Costs: {0}";
}
