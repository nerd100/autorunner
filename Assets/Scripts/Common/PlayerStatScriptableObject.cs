using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PlayerStats", order = 1)]
public class PlayerStatScriptableObject : ScriptableObject
{
    public int Id;
    public string Name;
    public PlayerStatsEnum playerStatEnum;
    public float Value;
    public int Costs;
    public string Title;
    public Sprite Sprite;
}
