﻿using UnityEngine;
using System;

public class EventManager : MonoBehaviour
{
    public static EventManager current;

    PlayerStatManager playerStatManager;
    GameManager gameManager;

    private void Awake()
    {
        current = this;
    }

    private void Start()
    {
        gameManager = GameManager.current;
        playerStatManager = PlayerStatManager.current;

        gameManager.OnClickOnScreen += IncreaseCurrencyOnClickEvent;
        gameManager.OnCollectTriggerEnter += ObjectCollectedEvent;
    }

    public void ObjectCollectedEvent(int value)
    {
        FindObjectOfType<AudioManager>().Play("CollectSound");
        PlayerStatManager.current.crystalCounter.Number += value * Mathf.RoundToInt(PlayerStatManager.current.CrystalMultiplier);
        GameManager.current.UpdatePlayerStats();
    }

    public void IncreaseCurrencyOnClickEvent()
    {
        var clickValue = playerStatManager.OnClickValue;
        playerStatManager.crystalCounter.Number += Mathf.RoundToInt(clickValue);
        GameManager.current.UpdatePlayerStats();
    }

}
