﻿
public enum GameState 
{
   BeforeStart,
   Started,
   Paused,
   Stopped
}
