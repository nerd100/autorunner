﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance = null;

    PlayerStatManager playerStatManager;
    GameManager gameManager;

    bool makeDistance = true;
    float maxSpeed = 100;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

    }

    // Start is called before the first frame update
    void Start()
    {
        playerStatManager = PlayerStatManager.current;
        gameManager = GameManager.current;
    }

    // Update is called once per frame
    void Update()
    {
        if(makeDistance)
        {
            StartCoroutine(MakeDistance());
            playerStatManager.distanceCounter.Number++;
        }
    }

    IEnumerator MakeDistance()
    {
        makeDistance = false;
        yield return new WaitForSeconds((maxSpeed-playerStatManager.PlayerSpeed)/100);
        makeDistance = true;
    }
}
