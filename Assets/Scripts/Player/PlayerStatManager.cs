﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class PlayerStatManager : MonoBehaviour, IPlayerStatManager
{
    public static PlayerStatManager current;

    public List<PlayerStatScriptableObject> statList = new List<PlayerStatScriptableObject>();

    public int CrystalCounter { get; set; }

    public int DistanceCounter { get; set; }


    //Stats
    public float PlayerSpeed { get; set; }

    public float PlayerSpeedCosts { get; set; }

    public float CrystalSpawnRate { get; set; }

    public float CrystalSpawnRateCosts { get; set; }

    public float CrystalUpgradeChance { get; set; }

    public float CrystalUpgradeChanceCosts { get; set; }

    public float CrystalMultiplier { get; set; }

    public float CrystalMultiplierCosts { get; set; }

    public float OnClickValue { get; set; }

    public float OnClickValueCosts { get; set; }


    //SpawnRates for WorldObjects
    public float CloudSpawnRate { get; set; }

    public float Environment1SpawnRate { get; set; }

    public float Environment2SpawnRate { get; set; }


    //ScriptableObjectStats
    public CounterScriptableObject crystalCounter;

    public CounterScriptableObject distanceCounter;

    public SpawnRateScriptableObject cloudSpawnRate;

    public SpawnRateScriptableObject environment1SpawnRate;

    public SpawnRateScriptableObject environment2SpawnRate;

    // Start is called before the first frame update
    void Awake()
    {
        current = this;

        this.UpdateStats();

        CloudSpawnRate = cloudSpawnRate.spawnRate;
        Environment1SpawnRate = environment1SpawnRate.spawnRate;
        Environment2SpawnRate = environment2SpawnRate.spawnRate;
        CrystalCounter = crystalCounter.Number;
        DistanceCounter = distanceCounter.Number;
    }

    public void Start()
    {
        GameManager.current.OnUpdatePlayerStats += UpdateStats;
    }

    public void UpdateStats()
    {
        foreach (var stat in statList)
        {
            if (stat != null)
            {
                switch (stat.playerStatEnum)
                {
                    case PlayerStatsEnum.PlayerSpeed:
                        PlayerSpeed = stat.Value;
                        CalculateCosts(stat);
                        PlayerSpeedCosts = stat.Costs;
                        break;
                    case PlayerStatsEnum.SpawnRate:
                        CrystalSpawnRate = stat.Value;
                        CalculateCosts(stat);
                        CrystalSpawnRateCosts = stat.Costs;
                        break;
                    case PlayerStatsEnum.CrystalUpgradeChance:
                        CrystalUpgradeChance = stat.Value;
                        CalculateCosts(stat);
                        CrystalUpgradeChanceCosts = stat.Costs;
                        break;
                    case PlayerStatsEnum.Multiplier:
                        CrystalMultiplier = stat.Value;
                        CalculateCosts(stat);
                        CrystalMultiplierCosts = stat.Costs;
                        break;
                    case PlayerStatsEnum.OnClickValue:
                        OnClickValue = stat.Value;
                        CalculateCosts(stat);
                        OnClickValueCosts = stat.Costs;
                        break;
                }
            }
            else
            {
                Debug.Log(stat.ToString() + " : Nicht gesetzt.");
            }
        }

        CloudSpawnRate = cloudSpawnRate.spawnRate;
        Environment1SpawnRate = environment1SpawnRate.spawnRate;
        Environment2SpawnRate = environment2SpawnRate.spawnRate;
        CrystalCounter = crystalCounter.Number;
    }

    private void Update()
    {
        DistanceCounter = distanceCounter.Number;
    }

    public void IncreaseStat(PlayerStatScriptableObject playerStat, int value)
    {
        playerStat.Value += value;
        CalculateCosts(playerStat);
    }

    private void CalculateCosts(PlayerStatScriptableObject playerStat)
    {
        switch (playerStat.playerStatEnum)
        {
            case PlayerStatsEnum.CrystalUpgradeChance:
                playerStat.Costs = (int)Mathf.Ceil(0.5f * Mathf.Pow(playerStat.Value, 2.00f));
                break;
            case PlayerStatsEnum.Multiplier:
                playerStat.Costs = (int)Mathf.Ceil(2f * Mathf.Pow(playerStat.Value, 2.00f));
                break;
            case PlayerStatsEnum.PlayerSpeed:
            case PlayerStatsEnum.SpawnRate:
            case PlayerStatsEnum.OnClickValue:
                playerStat.Costs = (int)Mathf.Ceil(0.005f * Mathf.Pow(playerStat.Value, 2.00f));
                break;
        }
    }

    public void DecreaseCurrency(int value)
    {
        crystalCounter.Number -= value;
    }
}
