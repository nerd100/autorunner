﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UpgradeStatsScript : MonoBehaviour
{
    public PlayerStatScriptableObject playerStatObject;
    [SerializeField]
    private TMP_Text title;
    [SerializeField]
    private Image image;
    [SerializeField]
    private TMP_Text currentValue;
    [SerializeField]
    private TMP_Text currentCosts;
    [SerializeField]
    private Button upgradeButton;

    private int currentCrystals;
    private PlayerStatManager playerStatManager;
    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.current;
        playerStatManager = PlayerStatManager.current;

        gameManager.OnUpdatePlayerStats += UpdateStats;
        currentCrystals = playerStatManager.CrystalCounter;

        title.text = playerStatObject.Title;
        image.sprite = playerStatObject.Sprite;
        currentValue.text = playerStatObject.Value.ToString();
        currentCosts.text = playerStatObject.Costs.ToString();
        if (playerStatObject.Costs > currentCrystals)
        {
            upgradeButton.interactable = false;
        }
        else
        {
            upgradeButton.interactable = true;
        }
    }

    public void UpdateStats()
    {
        currentCrystals = playerStatManager.CrystalCounter;
        currentValue.text = playerStatObject.Value.ToString();
        currentCosts.text = playerStatObject.Costs.ToString();

        if (playerStatObject.Costs > currentCrystals)
        {
            upgradeButton.interactable = false;
        }
        else
        {
            upgradeButton.interactable = true;
        }
    }
}
