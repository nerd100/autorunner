﻿using System.Collections.Generic;
using UnityEngine;

public class UpgradePlayerStats : MonoBehaviour
{
    private List<PlayerStatScriptableObject> currStatList = new List<PlayerStatScriptableObject>();

    PlayerStatManager playerStatManager;
    GameManager gameManager;
    EventManager eventManager;

    // Start is called before the first frame update
    void Start()
    {
        playerStatManager = PlayerStatManager.current;
        gameManager = GameManager.current;
        eventManager = EventManager.current;

        currStatList = playerStatManager.statList;
    }

    public void IncreaseByValue(PlayerStatScriptableObject playerStat)
    {
        if (playerStat != null)
        {
            playerStatManager.DecreaseCurrency(playerStat.Costs);
            gameManager.UpdatePlayerStats();
            playerStatManager.IncreaseStat(playerStat, 1);
            gameManager.UpdatePlayerStats();
        }
    }
}
