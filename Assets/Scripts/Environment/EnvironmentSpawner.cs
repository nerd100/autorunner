﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSpawner : MonoBehaviour
{
    public Vector3 spawnPosition = new Vector3(0, 0, 0);
    public float spawnRate = 1.0f;
    public bool spawnTrigger = true;
    public bool IsContainerReady = false;

    public ObjectPoolContainer objectPoolContainer;
    public SpawnRateScriptableObject spawnRateObject;

    //Randomize SpawnPosition
    public bool isRandomSpawnEnabled = false;
    public float positionOffsetXMin = 0;
    public float positionOffsetXMax = 0;
    public float positionOffsetYMin = 0;
    public float positionOffsetYMax = 0;

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.transform.position = spawnPosition;
        ObjectPooler.SharedInstance.CreatePoolForGameObject(objectPoolContainer, this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        spawnRate = spawnRateObject.spawnRate;
        if (this.gameObject.transform.childCount > 0)
        {
            IsContainerReady = true;
        }

        if (spawnTrigger && IsContainerReady)
        {
            StartCoroutine(SpawnObjectWithCooldown(spawnRate));
        }
    }

    public IEnumerator SpawnObjectWithCooldown(float coolDown)
    {
        spawnTrigger = false;
        yield return new WaitForSeconds((100 - spawnRate) / 100);
        var inactiveGameobject = FindNextInactiveGameObject();
        if (inactiveGameobject != null)
        {
            if (isRandomSpawnEnabled)
            {
                var offsetX = Random.Range(positionOffsetXMin, positionOffsetXMax);
                var offsetY = Random.Range(positionOffsetYMin, positionOffsetYMax);
                var objectPosition = inactiveGameobject.transform.position;
                inactiveGameobject.transform.position = new Vector3(objectPosition.x + offsetX, objectPosition.y + offsetY, objectPosition.z);
            }
            inactiveGameobject.SetActive(true);
        }
        spawnTrigger = true;
    }

    public GameObject FindNextInactiveGameObject()
    {
        GameObject nextInactiveGameObject = null;
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            nextInactiveGameObject = this.gameObject.transform.GetChild(i).gameObject;
            if (!nextInactiveGameObject.activeInHierarchy)
            {
                nextInactiveGameObject.transform.position = this.gameObject.transform.position;
                return nextInactiveGameObject;
            }
        }

        ObjectPooler.SharedInstance.ExtendPoolForGameObject(objectPoolContainer, this.gameObject, 5);

        return null;
    }
}
