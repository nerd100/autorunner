﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEnvironmentCollisionScript : MonoBehaviour
{
    //public GameObject relativObject;

    private float relativePositionZ = 0;

    private void Start()
    {
        relativePositionZ = 0;

    }

    void Update()
    {
        var objPositionZ = this.gameObject.transform.position.z;
        if (objPositionZ < relativePositionZ)
        {
            this.gameObject.SetActive(false);

        }
    }
}
