﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollisionScript : MonoBehaviour
{
    //public GameObject relativObject;

    private float relativePositionZ = 0;

    private void Start()
    {
        relativePositionZ = 0;      
    }

    void Update()
    {
        var objPositionZ = this.gameObject.transform.position.z;
        //Debug.Log(objPositionZ);
        if(objPositionZ < relativePositionZ)
        {
            var collectValue = this.gameObject.GetComponent<ObjectMovement>().value;
            GameManager.current.Collect(collectValue);
            this.gameObject.transform.position = new Vector3(0,0,0);
            this.gameObject.SetActive(false);
            
        }
    }
}
