﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public float velocity = 1.0f;
    public bool upgraded = false;
    public int value = 1;

    public PlayerStatScriptableObject playerSpeedStat;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        velocity = playerSpeedStat.Value;
        this.gameObject.transform.position += new Vector3(0, 0, -moveSpeed) * velocity * Time.deltaTime;
    }
}
