﻿

public interface IPlayerStatManager
{
    void IncreaseStat(PlayerStatScriptableObject playerStat, int value);

    void DecreaseCurrency(int value);
}

