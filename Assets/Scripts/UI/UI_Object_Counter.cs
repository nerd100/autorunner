﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_Object_Counter : MonoBehaviour
{
    [SerializeField]
    private readonly CounterScriptableObject crystalCounter;

    public TMP_Text crystalCount_MainScreen;
    public TMP_Text crystalCount_UpgradeScreen;

    public TMP_Text distanceCount_MainScreen;
    public TMP_Text distanceCount_UpgradeScreen;

    private void Awake()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        crystalCount_MainScreen.text = PlayerStatManager.current.CrystalCounter.ToString();
        crystalCount_UpgradeScreen.text = PlayerStatManager.current.CrystalCounter.ToString();

        distanceCount_MainScreen.text = PlayerStatManager.current.DistanceCounter.ToString();
        distanceCount_UpgradeScreen.text = PlayerStatManager.current.DistanceCounter.ToString();
    }
}
