using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelAnimationScript : MonoBehaviour
{

    bool openPanelTrigger = false;
    bool closePanelTrigger = false;


    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.transform.localScale = new Vector3(0.0f, 0.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(openPanelTrigger && this.gameObject.transform.localScale.x < 1.0f && this.gameObject.transform.localScale.y < 1.0f)
        {
            if(this.gameObject.transform.localScale.x < 1.0f)
            {
                this.gameObject.transform.localScale += new Vector3(0.1f, 0, 0);
            }

            if (this.gameObject.transform.localScale.y < 1.0f)
            {
                this.gameObject.transform.localScale += new Vector3(0, 0.1f, 0);
            }

            if(this.gameObject.transform.localScale.x >= 1.0f && this.gameObject.transform.localScale.y >= 1.0f)
            {
                openPanelTrigger = false;
            }
        }

        if (closePanelTrigger && this.gameObject.transform.localScale.x > 0f && this.gameObject.transform.localScale.y > 0f)
        {
            if (this.gameObject.transform.localScale.x > 0f)
            {
                this.gameObject.transform.localScale -= new Vector3(0.1f, 0, 0);
            }

            if (this.gameObject.transform.localScale.y > 0f)
            {
                this.gameObject.transform.localScale -= new Vector3(0, 0.1f, 0);
            }

            if (this.gameObject.transform.localScale.x <= 0f && this.gameObject.transform.localScale.y <= 0f)
            {
                closePanelTrigger = false;
                this.gameObject.SetActive(false);
            }
        }
    }

    public void OpenPanel()
    {
        this.gameObject.SetActive(true);
        openPanelTrigger = true;
    }

    public void ClosePanel()
    {
        closePanelTrigger = true;
    }
}
