﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomNavBarScript : MonoBehaviour
{
    public GameObject upgradePanel;
    public GameObject prestigePanel;
    public GameObject achievementPanel;
    public GameObject statsPanel;
    // Start is called before the first frame update

    private GameManager gameManager;


    void Start()
    {
        gameManager = GameManager.current;

        upgradePanel.gameObject.SetActive(false);
        prestigePanel.gameObject.SetActive(false);
        achievementPanel.gameObject.SetActive(false);
        statsPanel.gameObject.SetActive(false);
    }

    public void OnButtonPressOpen(int buttonEnum)
    {
        switch (buttonEnum)
        {
            case (int)ButtonEnum.UpgradeButton:
                gameManager.UpdatePlayerStats();
                upgradePanel.gameObject.SetActive(true);
                break;

            case (int)ButtonEnum.PrestigeButton:
                prestigePanel.gameObject.SetActive(true);
                break;

            case (int)ButtonEnum.AchievementsButton:
                achievementPanel.gameObject.SetActive(true);
                break;

            case (int)ButtonEnum.StatsButton:
                statsPanel.gameObject.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void ClosePanel(int buttonEnum)
    {
        switch (buttonEnum)
        {
            case (int)ButtonEnum.UpgradeButton:
                upgradePanel.gameObject.SetActive(false);
                break;

            case (int)ButtonEnum.PrestigeButton:
                prestigePanel.gameObject.SetActive(false);
                break;

            case (int)ButtonEnum.AchievementsButton:
                achievementPanel.gameObject.SetActive(false);
                break;

            case (int)ButtonEnum.StatsButton:
                statsPanel.gameObject.SetActive(false);
                break;

            default:
                break;
        }
    }
}
