﻿
public enum PlayerStatsEnum
{
    PlayerSpeed = 1,
    SpawnRate = 2,
    CrystalUpgradeChance = 3,
    Multiplier = 4,
    OnClickValue = 5
}

