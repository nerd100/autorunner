﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolItem
{
    public int containerId;
    public int amountToPool;
    public GameObject objectToPool;
    public bool shouldExpand;
}