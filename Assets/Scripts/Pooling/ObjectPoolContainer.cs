﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolContainer
{
    public int containerId;
    public int amountToPool;
    public GameObject objectToPool;
}
