﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler SharedInstance;

    void Awake()
    {
        SharedInstance = this;
    }

    public void CreatePoolForGameObject(ObjectPoolContainer objectPoolContainer, GameObject parentGameObject)
    {
        var container = objectPoolContainer;
        for (int i = 0; i < container.amountToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(container.objectToPool);
            obj.SetActive(false);
            obj.transform.parent = parentGameObject.transform;
            obj.transform.position = parentGameObject.transform.position;
        }
    }

    public void ExtendPoolForGameObject(ObjectPoolContainer objectPoolContainer, GameObject parentGameObject, int numberToExtend)
    {
        var container = objectPoolContainer;
        for (int i = 0; i < numberToExtend; i++)
        {
            GameObject obj = (GameObject)Instantiate(container.objectToPool);
            obj.SetActive(false);
            obj.transform.parent = parentGameObject.transform;
            obj.transform.position = parentGameObject.transform.position;
        }
    }
}

