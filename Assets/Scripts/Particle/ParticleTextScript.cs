﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ParticleTextScript : MonoBehaviour
{
    private PlayerStatManager playerStatManager;
    private Color textColor;

    private void Start()
    {
        playerStatManager = PlayerStatManager.current;
        var currentClickValue = playerStatManager.OnClickValue;
        this.gameObject.GetComponent<TextMeshProUGUI>().text = currentClickValue.ToString();
        Destroy(this.gameObject, 1.0f);

        textColor = new Color(1, 1, 1, 1);
    }
    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position += new Vector3(0, 0.5f, 0);
        this.gameObject.GetComponent<TextMeshProUGUI>().color = textColor;
        textColor.a -= 0.01f;
    }
}
