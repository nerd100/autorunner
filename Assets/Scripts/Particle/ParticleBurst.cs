﻿using UnityEngine;

public class ParticleBurst : MonoBehaviour
{
    private GameManager gameManager;
    public new ParticleSystem particleSystem;
    public GameObject TextParticle;
    Plane plane;
    public GameObject particleCanvasContainer;
    public GameObject particleSystemContainer;

    private void Start()
    {
        gameManager = GameManager.current;
        gameManager.OnClickOnScreen += ShootParticles;

        plane = new Plane(Vector3.forward, Vector3.zero);
    }

    private void ShootParticles()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Initialise the enter variable
        float enter = 0.0f;

        if (plane.Raycast(ray, out enter))
        {
            Vector3 hitPoint = ray.GetPoint(enter);
            
            var textParticle = Instantiate(TextParticle, Vector3.zero, Quaternion.identity);
            textParticle.transform.SetParent(particleCanvasContainer.transform, false);
            textParticle.transform.position = Input.mousePosition;

            var particleObject = Instantiate(particleSystem, Vector3.zero, Quaternion.identity);
            particleObject.transform.SetParent(particleSystemContainer.transform, false);
            particleObject.transform.position = hitPoint;
            particleObject.Play();

        }
    }

}

